use {std::error::Error, tokio::net::UdpSocket};

#[derive(Debug, Copy, Clone)]
enum QueryType {
    Question = 0,
    Response = 1,
}

#[derive(Debug, Copy, Clone)]
enum OpCode {
    Query,
    InverseQuery,
    Status,
    Notify,
    Update,
    DNSStatefulOperations,
    Unassigned,
}

#[derive(Debug, Copy, Clone)]
enum RCode {
    NoError,
    FormatError,
    ServerFailure,
    NameError,
    NotImplemented,
    Refused,
}

#[derive(Debug)]
pub struct DNSHeader {
    id: u16,
    query_type: QueryType,
    op_code: OpCode, // Only lower 4 bits are set here
    is_authoritative: bool,
    is_truncated: bool,
    recursion_desired: bool,
    recursion_available: bool,

    _z: u8, // TODO

    r_code: RCode,

    qd_count: u16,
    an_count: u16,
    ns_count: u16,
    ar_count: u16,
}

impl DNSHeader {
    pub fn parse(buf: &[u8]) -> Result<DNSHeader, Box<dyn Error>> {
        if buf.len() < 12 {
            return Err("buffer length is less than 12 bytes".into());
        }

        let id = u16::from_be_bytes([buf[0], buf[1]]);

        // Byte 2 ops
        let query_type = if buf[2] & 0b10000000 == 1 {
            QueryType::Response
        } else {
            QueryType::Question
        };
        let op_code = match (buf[2] & 0b01111000) >> 3 {
            0 => OpCode::Query,
            1 => OpCode::InverseQuery,
            2 => OpCode::Status,
            4 => OpCode::Notify,
            5 => OpCode::Update,
            6 => OpCode::DNSStatefulOperations,
            3 | 7..=15 => OpCode::Unassigned,

            _ => unreachable!("unreachable code in DNS op_code parser"),
        };
        let is_authoritative = buf[2] & 1 << 4 == 1;
        let is_truncated = buf[2] & 1 << 3 == 1;
        let recursion_desired = buf[2] & 1 << 2 == 1;
        // Byte 2 ops end here

        // Byte 3 ops
        let recursion_available = buf[3] & 0b10000000 == 1;
        //        self._z = buf[3] & 0b01110000;
        let r_code = match buf[3] & 0b00001111 {
            0 => RCode::NoError,
            1 => RCode::FormatError,
            2 => RCode::ServerFailure,
            3 => RCode::NameError,
            4 => RCode::NotImplemented,
            5 => RCode::Refused,
            6..=15 => unimplemented!(),
            _ => unreachable!("unreachable code in DNS r_code parser"),
        };

        let qd_count = u16::from_be_bytes([buf[4], buf[5]]);
        let an_count = u16::from_be_bytes([buf[6], buf[7]]);
        let ns_count = u16::from_be_bytes([buf[8], buf[9]]);
        let ar_count = u16::from_be_bytes([buf[10], buf[11]]);

        Ok(Self {
            id,
            query_type,
            op_code,
            is_authoritative,
            is_truncated,
            recursion_desired,
            recursion_available,
            r_code,
            qd_count,
            an_count,
            ns_count,
            ar_count,

            _z: 0,
        })
    }
}

#[tokio::main]
async fn main() -> std::io::Result<()> {
    //    const ADDR: &'static str = "[::]:3000";
    const ADDR: &'static str = "127.0.0.1:5353";
    const UPSTREAM: &'static str = "8.8.8.8:53";

    let mut socket = UdpSocket::bind(ADDR).await?;

    println!("Started listening on {}", ADDR);
    loop {
        let mut clientbuf = [0; 4096];
        let mut dstbuf = [0; 4096];

        let (amt, client) = socket
            .recv_from(&mut clientbuf)
            .await
            .map_err(|err| format!("error in receiving from client socket: {}", err))
            .unwrap();

        let header = DNSHeader::parse(&clientbuf[0..=12]);

        println!("header {:?}", header);

        println!(
            "{:?}",
            &clientbuf[..amt]
                .iter()
                .map(|x| format!("  {:08b}  ", x))
                .collect::<String>()
        );
    }
}
