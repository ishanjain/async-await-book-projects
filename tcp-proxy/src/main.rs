use tokio::{
    io::{self},
    net::{TcpListener, TcpStream},
    select,
};

const SERVER: &str = "dns.ishan.pw:443";

#[tokio::main]
async fn main() -> std::io::Result<()> {
    const ADDR: &'static str = "localhost:4000";

    let mut listener = TcpListener::bind(ADDR).await?;

    println!("Started listening on {}", ADDR);
    loop {
        let (socket, _) = listener.accept().await?;
        process_req(socket).await?;
    }
}

async fn process_req(client: TcpStream) -> std::io::Result<()> {
    println!("Connected to {:?}", client.peer_addr().unwrap());

    // Dial to the server and return the TCP Connection
    let origin = TcpStream::connect(SERVER).await?;

    let (mut origin_read, mut origin_write) = origin.into_split();
    let (mut client_read, mut client_write) = client.into_split();

    // Copy bytes from client to origin and origin to client
    let o2c = tokio::spawn(async move { io::copy(&mut client_read, &mut origin_write).await });
    let c2o = tokio::spawn(async move { io::copy(&mut origin_read, &mut client_write).await });

    // If origin or client close connection, We should also close the remaining connection
    select! {
        _ = c2o => println!("client->origin done"),
        _ = o2c => println!("origin->client done"),
    }

    Ok(())
}
